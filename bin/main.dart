import 'dart:convert';

import 'package:puppeteer/puppeteer.dart';
import 'package:puppeteer_util/puppeteer_util.dart';

// dart run bin/main.dart https://dart.dev >dart_pptr.log
// dart run bin/main.dart https://www.lokinet.org >lokinet_pptr.log
void main(List<String> args) async {
  // Download the Chrome binaries, launch it and connect to the "DevTools"
  var browser = await puppeteer.launch(
      // headless: false,
      waitForInitialPage: false,
      defaultViewport: null,
      ignoreDefaultArgs: [
        "--enable-automation"
      ],
      args: [
        "--incognito",
        "--disable-background-timer-throttling",
        "--disable-backgrounding-occluded-windows",
        "--disable-breakpad",
        "--disable-component-extensions-with-background-pages",
        "--disable-dev-shm-usage",
        "--disable-extensions",
        "--disable-features=TranslateUI,BlinkGenPropertyTrees",
        "--disable-ipc-flooding-protection",
        "--disable-renderer-backgrounding",
        "--enable-features=NetworkService,NetworkServiceInProcess",
        "--force-color-profile=srgb",
        "--hide-scrollbars",
        "--metrics-recording-only",
        "--mute-audio",
        "--no-sandbox",
        "--window-size=1920,1080",
        "--blink-settings=imagesEnabled=false",
      ]);

  // Open a new tab
  final myPage = await browser.newPage();
  try {
    await myPage.setRequestInterception(true);
    await myPage.setUserAgent(
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36 Edg/101.0.1210.53");
    await myPage.evaluateOnNewDocument(
        'Object.defineProperty(navigator,"webdriver",{get:()=>undefined,});');

    myPage.onRequest.listen((req) {
      req.continueRequest();
    });

    myPage.onRequestFinished.listen((req) async {
      final resp = req.response!;
      resp.bytes.then((body) {
        if (body.isNotEmpty) {
          printFinishedRequestInfo(req, resp, body);
        }
      }).catchError((err) {
        printFinishedRequestInfo(req, resp, utf8.encode("(nobody)"));
      });
    });

    await myPage.goto(args[0]);
  } finally {
    await myPage.close();
    // Gracefully close the browser's process
    await browser.close();
  }
}
