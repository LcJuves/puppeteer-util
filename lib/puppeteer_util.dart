import 'dart:convert';

import 'package:puppeteer/puppeteer.dart';

void printFinishedRequestInfo(Request req, Response resp, List<int> body) {
  print("${req.method} ${req.url}");
  for (final headerEntry in req.headers.entries) {
    final headerKey = headerEntry.key;
    print("$headerKey: ${headerEntry.value}");
  }
  print("");
  if (req.postData != null && req.postData!.isNotEmpty) {
    print(req.postData);
    print("");
  }

  print("<<<<<<");
  print("${resp.status} ${resp.statusText}");
  for (final headerEntry in resp.headers.entries) {
    final headerKey = headerEntry.key;
    print("$headerKey: ${headerEntry.value}");
  }
  print("");

  print(utf8.decode(body));
  print("============================================================");
  print("============================================================");
  print("");
}
